﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;

namespace ConsoleApplication2
{
    public enum ValueType
    {
        General,
        Text,
        Date
    }

    class Program
    {
        static Application excel;
        static Workbook workBook;
        static Worksheet workSheet;
        static Range range;

        static string tableName;
        static List<string> columnNames = new List<string>();
        static List<ValueType> columnTypes = new List<ValueType>();
        static List<List<string>> values = new List<List<string>>();

        static void Main( string[] args )
        {
            if ( args.Length != 1 )
            {
                Version v = Assembly.GetExecutingAssembly().GetName().Version;
                Console.WriteLine( "Excel2SqlInsert v{0}.{1}.{2}", v.Major, v.Minor, v.Build );
                Console.WriteLine( "----------------------\n" );
                Console.WriteLine( "Drop an Excel file onto it's icon in the Windows Explorer.\n\n" );
                Console.WriteLine( "Content" );
                Console.WriteLine( "-------\n" );
                Console.WriteLine( "Cell A1: Table name" );
                Console.WriteLine( "Row 2:   Field names" );
                Console.WriteLine( "Row 3:   Data types for each field: 'Text', 'Date' or empty\n" );
                Console.WriteLine( "Press any key." );
                Console.ReadKey();
                return;
            }

            if ( !File.Exists( args[ 0 ] ) )
            {
                Console.WriteLine( "File not found" );
                Console.ReadKey();
                return;
            }

            try
            {
                excel = new Application();
                workBook = excel.Workbooks.Open( args[ 0 ] );
                workSheet = (Worksheet)workBook.Worksheets.get_Item( 1 );

                range = workSheet.UsedRange;

                ReadTableName();
                ReadColumnNames();
                ReadColumnTypes();
                ReadValues();

                var query = BuildQuery();

                WriteToFile( args[ 0 ], query );
            }
            finally
            {
                ReleaseObject( range );
                ReleaseObject( workSheet );
                ReleaseObject( workBook );

                excel.Application.Quit();

                ReleaseObject( excel );
            }

            Console.WriteLine( "\nDone! Press any key." );
            Console.ReadKey();
        }

        private static void ReadTableName()
        {
            tableName = (string)( range.Cells[ 1, 1 ] as Range ).Value2;

            Console.WriteLine( "Table:   {0}", tableName );
        }

        private static void ReadColumnNames()
        {
            for ( int c = 0; c < range.Columns.Count; c++ )
            {
                string value = ( range.Cells[ 2, c + 1 ] as Range ).Value2;
                if ( string.IsNullOrEmpty( value ) )
                {
                    break;
                }
                else
                {
                    columnNames.Add( value );
                }
            }

            Console.WriteLine( "Columns: {0}", columnNames.Count );
        }

        private static void ReadColumnTypes()
        {
            for ( int c = 0; c < columnNames.Count; c++ )
            {
                string value = ( range.Cells[ 3, c + 1 ] as Range ).Value2;

                ValueType type = ValueType.General;

                if ( !string.IsNullOrEmpty( value ) )
                {
                    type = (ValueType)Enum.Parse( typeof( ValueType ), value, true );
                }

                columnTypes.Add( type );
            }
        }

        private static void ReleaseObject( object obj )
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject( obj );
                obj = null;
            }
            catch ( Exception ex )
            {
                obj = null;
                Console.Write( "Unable to release the Object " + ex.ToString() );
                Console.ReadKey();
            }
            finally
            {
                GC.Collect();
            }
        }

        private static void ReadValues()
        {
            for ( int r = 4; r < range.Rows.Count + 1; r++ )
            {
                List<string> row = new List<string>();

                for ( int c = 0; c < columnNames.Count; c++ )
                {
                    object content = ( range.Cells[ r, c + 1 ] as Range ).Value2;
                    string value = "";

                    if ( content != null )
                    {
                        ValueType type = columnTypes[ c ];

                        value = ( range.Cells[ r, c + 1 ] as Range ).Value2.ToString().Trim();

                        switch ( type )
                        {
                            case ValueType.Date:
                                value = ConvertDate( value );
                                break;

                            case ValueType.Text:
                                break;
                        }
                    }

                    row.Add( value );
                }

                values.Add( row );
            }

            Console.WriteLine( "Rows:    {0}", values.Count() );
        }

        private static string ConvertDate( string value )
        {
            if ( !string.IsNullOrEmpty( value ) )
            {
                double julDate = Convert.ToDouble( value );
                DateTime date = DateTime.FromOADate( julDate );
                value = date.ToString( "d-MMM-yyyy", CultureInfo.InvariantCulture );
            }
            return value;
        }

        private static string BuildQuery()
        {
            StringBuilder sb = new StringBuilder();

            foreach ( var row in values )
            {
                sb.AppendFormat( "INSERT INTO {0} (", tableName );

                foreach ( var field in columnNames )
                {
                    sb.AppendFormat( "{0}, ", field );
                }

                sb.Remove( sb.Length - 2, 2 );
                sb.Append( " )\n    VALUES (" );

                for ( int i = 0; i < row.Count; i++ )
                {
                    if ( columnTypes[ i ] == ValueType.Text
                        || columnTypes[ i ] == ValueType.Date )
                    {
                        sb.AppendFormat( "'{0}', ", row[ i ] );
                    }
                    else
                    {
                        sb.AppendFormat( "{0}, ", row[ i ] );
                    }
                }

                sb.Remove( sb.Length - 2, 2 );
                sb.Append( " );\n\n" );
            }

            string s = sb.ToString();
            return s;
        }

        private static void WriteToFile( string sourceFile, string query )
        {
            string dir = Path.GetDirectoryName( sourceFile );
            string baseName = Path.GetFileNameWithoutExtension( sourceFile );
            string fileName = baseName + ".sql";
            string path = Path.Combine( dir, fileName );

            File.WriteAllText( path, query );
        }

    }
}
